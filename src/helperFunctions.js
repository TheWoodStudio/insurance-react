export function getYearDifference(year) {
    return new Date().getFullYear() - year;
}

export function calculateBrand(brand) {
    let increase;

    switch (brand) {
        case 'European':
            increase = 1.30;
            break;
        case 'American':
            increase = 1.15;
            break;
        case 'Asian':
            increase = 1.05;
            break;
        default:
            break;
    }

    return increase;
}

export function getPlan(plan) {
    return (plan === 'Basic') ? 1.20 : 1.50;
}
