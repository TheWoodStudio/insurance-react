import React, { Component } from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

class Result extends Component {
    render() {
        const result = this.props.result;
        const message = (!result) ? 'Please, pick a brand, a year and a plan for your vehicle' : 'The price is $';

        return (
            <div className="total">
                {message}
                <TransitionGroup component="span" className="result">
                    <CSSTransition
                        classNames="result"
                        key={result}
                        timeout={{ enter: 500, exit: 500 }}>
                        
                        <span>{result}</span>

                    </CSSTransition>
                </TransitionGroup>
            </div>
        );
    }
}

export default Result;