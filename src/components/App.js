import React, { Component } from 'react';
import Header from './Header';
import Form from './Form';
import Summary from './Summary';
import Result from './Result';

import { getYearDifference, calculateBrand, getPlan } from '../helperFunctions';

class App extends Component {

  state = {
    result: '',
    data: {}
  }

  quoteInsurance = (data) => {
    const { brand, year, plan } = data;

    //Agregar una base de 2000
    let result = 2000;

    //Obtener la diferencia de años 
    const difference = getYearDifference(year);

    //y por cada año restar el 3%
    result -= ((difference * 3) * result) / 100;
    
    //Americano 15%, Asiático 5%, Europeo 30% de incremento al valor actual
    result = calculateBrand(brand) * result;

    //plans: basic + 20% - full + 50%
    let planIncrease = getPlan(plan);
    result = parseFloat( planIncrease * result ).toFixed(2);

    //¿No es lo mismo poner 'data' directamente en el estado?
    const carData = {
      brand: brand,
      year: year,
      plan: plan
    }
    //listo el costo del seguro.
    this.setState({
      result : result,
      data : carData
    })
  }

  render() {
    return (
      <div className="container">
        <Header
          titulo = 'Insurance Quotation'
        />

        <div className="small-container">
          <Form
            quoteInsurance={ this.quoteInsurance }
          />
          <Summary
            data={this.state.data}
          />
          <Result
            result={this.state.result}
          />
        </div>
      </div>
    );
  }
}

export default App;
