import React, { Component } from 'react';

class Summary extends Component {
    showSummary = () => {
        const { brand, year, plan } = this.props.data;

        if (!brand || !year || !plan) return null;

        return (
            <div className="summary">
                <h3>Quotation Summary</h3>
                <ul>
                    <li><strong>Brand: </strong>{brand}</li>
                    <li><strong>Year: </strong>{year}</li>
                    <li><strong>Plan: </strong>{plan}</li>
                </ul>
            </div>
        );
    }
    render() {
        
        return (
            <div>
                {this.showSummary()}
            </div>
        );
    }
}

export default Summary;