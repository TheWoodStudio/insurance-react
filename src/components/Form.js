import React, { Component } from 'react';

class Form extends Component {
    //refs son para leer los valores de los campos de un form, por ejemplo
    brandRef = React.createRef();
    yearRef = React.createRef();
    basicRef = React.createRef();
    fullRef = React.createRef();

    quoteInsurance = (e) => { //Para que no tener que hacer bind(this) en cada elemento
        e.preventDefault();
        
        //qué plan
        const selectedPlan = this.basicRef.current.checked ? 'Basic' : 'Full';
        //obtener los datos
        // console.log(this.brandRef.current.value);

        //crear el objeto
        const carInfo = {
            brand: this.brandRef.current.value,
            year: this.yearRef.current.value,
            plan: selectedPlan
        }

        //enviarlo al componente principal
        this.props.quoteInsurance(carInfo);
    }

    render() {
        return (
            <form onSubmit={this.quoteInsurance}>
                <div className="field">
                    <label>Brand</label>
                    <select name="brand" ref={this.brandRef}>
                        <option value="">Select an option</option>
                        <option value="American">American</option>
                        <option value="European">European</option>
                        <option value="Asian">Asian</option>
                    </select>
                </div>

                <div className="field">
                    <label>Year</label>
                    <select name="year" ref={this.yearRef}>
                        <option value="">Select an option</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                        <option value="2015">2015</option>
                        <option value="2014">2014</option>
                        <option value="2013">2013</option>
                        <option value="2012">2012</option>
                        <option value="2011">2011</option>
                        <option value="2010">2010</option>
                        <option value="2009">2009</option>
                        <option value="2008">2008</option>
                    </select>
                </div>

                <div className="field inputs">
                    <label>Plan</label>
                    <input type="radio" name="plan" value="Basic" ref={this.basicRef} /> Basic
                    <input type="radio" name="plan" value="Full" ref={this.fullRef} /> Full
                </div>

                <button type="submit">Quote</button>
            </form>
        );
    }
}

export default Form;